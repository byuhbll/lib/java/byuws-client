package edu.byu.hbll.byuws.client.services.access;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.byuws.client.ByuwsClient;
import edu.byu.hbll.byuws.client.ByuwsClientException;
import java.net.URI;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

/** Client for making requests to the BYU isMember service. */
@AllArgsConstructor
@Deprecated
public class IsMemberClient {

  private static final String GROUP = "group";
  private static final String USER = "user";
  private static final String SERVICE_PATH =
      String.format("domains/legacy/identity/access/ismember/v1/{%s}/{%s}", GROUP, USER);

  private final ByuwsClient client;

  /**
   * Invokes the isMember service to determine whether the given user belongs to the given group.
   *
   * @param group the group being queried for membership
   * @param user the user being queried for membership
   * @return {@code true} if the user belongs to the group, {@code false} otherwise
   * @throws ByuwsClientException if the web service fails
   */
  public boolean isMember(String group, String user) throws ByuwsClientException {
    URI endpoint =
        UriComponentsBuilder.fromUriString(SERVICE_PATH)
            .uriVariables(Map.of(GROUP, group, USER, user))
            .build()
            .toUri();

    JsonNode response = client.exchange(endpoint, HttpMethod.GET, null, JsonNode.class);
    return response.path("isMember Service").path("response").path("isMember").asBoolean();
  }
}
