package edu.byu.hbll.byuws.client.model.academic;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.Getter;

/** Jackson bean representing the control dates for a given semester. */
@Getter
public class ControlDates {

  private static final DateTimeFormatter DATE_FORMATTER =
      DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss");

  @JsonAlias("date_type")
  private Type type;

  @JsonAlias("year_term")
  private Semester semester;

  @JsonAlias("start_date")
  @JsonDeserialize(using = StartDateDeserializer.class)
  private LocalDateTime startDate;

  @JsonAlias("end_date")
  @JsonDeserialize(using = EndDateDeserializer.class)
  private LocalDateTime endDate;

  @JsonAlias("description")
  private String description;

  /** Jackson deserializer that parses the unusual date format used by the control dates service. */
  public static class StartDateDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser parser, DeserializationContext context)
        throws IOException, JsonProcessingException {
      return DATE_FORMATTER.parse(parser.readValueAs(String.class), LocalDateTime::from);
    }
  }

  /** Jackson deserializer that parses the unusual date format used by the control dates service. */
  public static class EndDateDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser parser, DeserializationContext context)
        throws IOException, JsonProcessingException {
      return DATE_FORMATTER
          .parse(parser.readValueAs(String.class), LocalDateTime::from)
          /**
           * The webservice only has a resolution of 1 second, but the Java time library has a
           * resolution of 1 nanosecond. Pad the end_date such that any Instant occurring during the
           * last second of the control date range will be included in the resulting "semester"
           * (e.g.: 12:00:00 pads to 12:00:00.999999999).
           */
          .plusSeconds(1L)
          .minusNanos(1L);
    }
  }

  /** Enumeration of possible control date types. */
  public enum Type {

    /** Date range beginning on the first day of classes and ending on the last day of classes. */
    CLASS_DATES,

    /**
     * Date range beginning on the day after the last day of the previous semester's finals and
     * ending on the last day of finals. This type maps each moment on the timeline to one and only
     * one semester.
     */
    CURRENT_YYT,

    /**
     * Date range beginning the first day of classes and ending on the day before the first day of
     * classes of the next semester. This type maps each moment on the timeline to one and only one
     * semester.
     */
    CURRICULUM,

    /** Date range beginning on the first day of finals and ending on the last day of finals. */
    FINAL_DATES,

    /**
     * Date range beginning on the date that the class schedule was published for this semester and
     * having a {@code null} ending date.
     */
    PUBLIC_VIEW,

    /**
     * Date range beginning when registration for the semester begins and ending on the add/drop
     * deadline.
     */
    REGISTRATION,

    /** Date range beginning on the first day of classes and ending on the last day of finals. */
    SEMESTER;
  }
}
