package edu.byu.hbll.byuws.client.services.identity;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.byuws.client.ByuwsClient;
import edu.byu.hbll.byuws.client.ByuwsClientException;
import java.net.URI;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

@AllArgsConstructor
@Deprecated
public class IdentityClient {

  private static final String RESOURCE_TYPE = "resourceType";
  private static final String IDENTIFIER = "identifier";
  private static final String SERVICE_PATH =
      String.format("domains/legacy/identity/person/PRO/{%s}/v1/{%s}", RESOURCE_TYPE, IDENTIFIER);

  private final ByuwsClient client;

  /**
   * Retrieves the person summary data for the given person.
   *
   * @param identifier the Person ID or Net ID of the person for which to retrieve data
   * @return the person summary data
   * @throws ByuwsClientException if the service fails
   */
  public JsonNode personSummary(String identifier) throws ByuwsClientException {
    return identityService(identifier, ResourceType.SUMMARY);
  }

  /**
   * Retrieves the person names data for the given person.
   *
   * @param identifier the Person ID or Net ID of the person for which to retrieve data
   * @return the person names data
   * @throws ByuwsClientException if the service fails
   */
  public JsonNode personNames(String identifier) throws ByuwsClientException {
    return identityService(identifier, ResourceType.NAMES);
  }

  /**
   * Helper method which invokes an identity service.
   *
   * @param identifier the Person ID or Net ID of the person for which to retrieve data
   * @param resourceType the type of person data to retrieve
   * @return the retrieved data
   * @throws ByuwsClientException if the service fails
   */
  private JsonNode identityService(String identifier, ResourceType resourceType)
      throws ByuwsClientException {

    URI endpoint =
        UriComponentsBuilder.fromUriString(SERVICE_PATH)
            .uriVariables(Map.of(RESOURCE_TYPE, resourceType.pathFragment, IDENTIFIER, identifier))
            .build()
            .toUri();

    // The legacy identity services are not compliant with the JSON specification.  Use the
    // ISO-8859-1 workaround method instead.
    JsonNode response = client.exchangeIso8859Json(endpoint, HttpMethod.GET, null, JsonNode.class);
    return response.path(resourceType.responseRoot).path("response");
  }

  /** * An enumeration of resource types currently supported by this client. */
  @AllArgsConstructor
  private enum ResourceType {
    SUMMARY("personsummary", "PersonSummaryService"),
    NAMES("personnames", "PersonNamesService");

    private final String pathFragment;
    private final String responseRoot;
  }
}
