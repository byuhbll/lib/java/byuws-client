package edu.byu.hbll.byuws.client.model.academic;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.List;
import lombok.Getter;

@Getter
public class Enrollment {

  @JsonAlias("year_term")
  @JsonDeserialize(using = Semester.Deserializer.class)
  private Semester semester;

  @JsonAlias("byu_id")
  private String byuId;

  @JsonAlias("sort_name")
  private String sortName;

  @JsonAlias("surname")
  private String surname;

  @JsonAlias("preferred_first_name")
  private String preferredFirstName;

  @JsonAlias("email_address")
  private String emailAddress;

  @JsonAlias("work_email_address")
  private String workEmailAddress;

  @JsonAlias("student_class_count")
  private int studentClassCount;

  @JsonAlias("student_class_list")
  private List<EnrollmentStudentClass> studentClasses;

  @JsonAlias("instructor_class_count")
  private int instructorClassCount;

  @JsonAlias("instructor_class_list")
  private List<EnrollmentInstructorClass> instructorClasses;
}
