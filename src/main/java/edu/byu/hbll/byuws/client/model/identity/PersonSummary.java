package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import java.util.List;
import lombok.Getter;

/** Defines the response returned from BYU's personPRO Person Summary service. */
@Getter
@Deprecated
public class PersonSummary {

  @JsonAlias("contact_information")
  private PersonSummaryContactInformation contactInformation;

  @JsonAlias("person_summary_line")
  private PersonSummaryLine personSummaryLine;

  @JsonAlias("employee_information")
  private PersonSummaryEmployeeInformation employeeInformation;

  @JsonAlias("identifiers")
  private PersonSummaryIdentifiers identifiers;

  @JsonAlias("names")
  private PersonSummaryNames names;

  @JsonAlias("student_information")
  private PersonSummaryStudentInformation studentInformation;

  @JsonAlias("personal_information")
  private PersonSummaryPersonalInformation personalInformation;

  @JsonAlias("relationships")
  private List<PersonSummaryRelationship> relationships;
}
