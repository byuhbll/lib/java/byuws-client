package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
@Deprecated
public class PersonSummaryLine {

  /**
   * An indicator that a person record is currently in the process of being merged with another
   * person record. Merging person records is a way of cleaning up identities or persons by
   * eliminating duplicates records.
   *
   * @return is merge pending
   */
  @JsonAlias("merge_pending")
  private boolean mergePending;

  /**
   * The 9-digit, unique, internal OIT identifier used by some legacy systems. The Person ID is not
   * to be used at points of service. Any system created in 2016 or after should use BYU ID instead
   * of Person ID for system-to-system identification.
   *
   * @return person ID
   */
  @JsonAlias("person_id")
  private String personId;

  /**
   * Returns a flag indicating whether a person has requested anonymity in the university's Identity
   * system. The Restricted Flag carries more serious implications than the Unlisted Data Flag: if a
   * person is marked as "restricted," his or her presence at the University MUST NOT be made known
   * to the public.
   *
   * @return is restricted
   */
  @JsonAlias("restricted")
  private boolean restricted;

  /**
   * Returns an indicator regarding whether a person has a BYU academic record.
   *
   * @return is academic record
   */
  @JsonAlias("academic_record")
  private boolean academicRecord;

  /**
   * Returns a unique identifier used for enterprise sign-in (with a password and/or other security
   * provisions such as two-factor authentication). Usually five-to-eight characters, beginning with
   * a letter. May be self-selected or system-generated. Net IDs are known for being more
   * human-readable than the other unique identifiers used at BYU.
   *
   * @return net ID
   */
  @JsonAlias("net_id")
  private String netId;

  /**
   * Designation of whether a person is currently an employee or a BYU affiliated employee (e.g.,
   * LDS Philanthropies, ROTC, etc.). The two values are Active and Inactive. The "Restricted Flag"
   * will restrict access where the restricted flag has been selected.
   *
   * @return is employee
   */
  @JsonAlias("is_employee")
  private boolean isEmployee;

  /**
   * The combination of Employee Classification Code, Employee Status Code and Employee Standing
   * Code that are each separated by a forward slash.
   *
   * @return employee role
   */
  @JsonAlias("employee_role")
  private String employeeRole;

  /**
   * The name of the person.
   *
   * @return name
   */
  @JsonAlias("name")
  private String name;

  /**
   * The nine-digit computer-generated ID assigned to each identity in BYU-IAM, which is printed on
   * the BYU ID Card in the format xx-xxx-xxxx and serves as a unique identifier. The BYU ID should
   * be used for identification at points of service, as well as for system-to-system
   * identification. Note: Although "BYU ID" is marked public, the BYU IDs of restricted persons are
   * not. For more information, see Restricted Person.
   *
   * @return BYU ID
   */
  @JsonAlias("byu_id")
  private String byuId;

  @JsonAlias("registrar_warning")
  private boolean registrarWarning;

  /**
   * Indicates whether the person is deceased. Note: Although "Deceased" is marked public, it is not
   * public for restricted persons. For more information, see Restricted Person.
   *
   * @return is deceased
   */
  @JsonAlias("deceased")
  private boolean deceased;

  /**
   * Flag showing if a particular identity is classified as an organization (i.e. non-person
   * organization).
   *
   * @return is non-person organization
   */
  @JsonAlias("non_person_organization")
  private boolean nonPersonOrganization;

  /**
   * Classification of students regarding how many credits they have earned toward their
   * undergraduate degree or which graduate-level program they are enrolled in. BYU has designated
   * this as directory information that it may disclose to the public without the student's written
   * consent. (The classification, not the credit hours a student has, is "directory.")
   *
   * @return student role
   */
  @JsonAlias("student_role")
  private String studentRole;
}
