package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
@Deprecated
public class PersonSummaryIdentifiers {

  /**
   * The 9-digit, unique, internal OIT identifier used by some legacy systems. The Person ID is not
   * to be used at points of service. Any system created in 2016 or after should use BYU ID instead
   * of Person ID for system-to-system identification.
   *
   * @return person ID
   */
  @JsonAlias("person_id")
  private String personId;

  /**
   * The two-digit number which is incremented each time a new ID Card is issued.
   *
   * @return BYU ID issue number
   */
  @JsonAlias("byu_id_issue_number")
  private String byuIdIssueNumber;

  /**
   * The person's social security number.
   *
   * @return social security number
   */
  @JsonAlias("ssn")
  private String ssn;

  /**
   * The nine-digit computer-generated ID assigned to each identity in BYU-IAM, which is printed on
   * the BYU ID Card in the format xx-xxx-xxxx and serves as a unique identifier. The BYU ID should
   * be used for identification at points of service, as well as for system-to-system
   * identification.
   *
   * @return BYU ID
   */
  @JsonAlias("byu_id")
  private String byuId;

  /**
   * Returns a unique identifier used for enterprise sign-in (with a password and/or other security
   * provisions such as two-factor authentication). Usually five-to-eight characters, beginning with
   * a letter. May be self-selected or system-generated. Net IDs are known for being more
   * human-readable than the other unique identifiers used at BYU.
   *
   * @return net ID
   */
  @JsonAlias("net_id")
  private String netId;
}
