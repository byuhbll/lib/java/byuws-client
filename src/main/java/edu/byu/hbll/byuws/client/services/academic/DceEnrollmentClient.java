package edu.byu.hbll.byuws.client.services.academic;

import edu.byu.hbll.byuws.client.ByuwsClient;
import edu.byu.hbll.byuws.client.ByuwsClientException;
import java.net.URI;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Client for making requests to the Department of Continuing Education (DCE) Enrollment Service.
 */
@AllArgsConstructor
public class DceEnrollmentClient {

  private static final String NET_ID = "netId";
  private static final String DEPARTMENTS = "departments";
  private static final String TYPES = "types";
  private static final String SERVICE_PATH =
      String.format(
          "/domains/DCE/EnrollStatus/v1.0/isEnrolled/netid/{%s}/departments/{%s}/types/{%s}",
          NET_ID, DEPARTMENTS, TYPES);

  private final ByuwsClient client;

  /**
   * Convenience method to check if a user is enrolled in University coursed at Independent Study.
   *
   * @param netId the Net ID of the person to check
   * @return whether or not the user is enrolled
   * @throws ByuwsClientException if the service fails
   */
  public boolean isEnrolledInIndependentStudy(String netId) throws ByuwsClientException {
    return this.isEnrolled(netId, Department.INDEPENDENT_STUDY, Type.UNIVERSITY);
  }

  /**
   * Retrieves the enrollment status for the given netId and DCE department.
   *
   * @param netId the Net ID of the person for which to retrieve data
   * @param department the DCE department to check enrollment
   * @return whether or not the user is enrolled
   * @throws ByuwsClientException if the service fails
   */
  public boolean isEnrolled(String netId, Department department) throws ByuwsClientException {
    return this.isEnrolled(netId, department, Type.ANY);
  }

  /**
   * Retrieves the enrollment status for the given netId, DCE Department, and Type.
   *
   * @param netId the Net ID of the person for which to retrieve data
   * @param department the DCE department to check enrollment
   * @param type the DCE type of enrollment to check
   * @return whether or not the user is enrolled
   * @throws ByuwsClientException if the service fails
   */
  public boolean isEnrolled(String netId, Department department, Type type)
      throws ByuwsClientException {

    URI endpoint =
        UriComponentsBuilder.fromUriString(SERVICE_PATH)
            .uriVariables(Map.of(NET_ID, netId, DEPARTMENTS, department, TYPES, type))
            .build()
            .toUri();
    return client.exchange(endpoint, HttpMethod.GET, null, Boolean.class);
  }

  /** An enumeration of departments in the Department of Continuing Education. */
  @AllArgsConstructor
  public enum Department {
    INDEPENDENT_STUDY("IS"),
    CONFERENCES_AND_WORKSHOPS("CW"),
    GENERAL_STUDIES("BGS");

    private final String name;

    @Override
    public String toString() {
      return name;
    }
  }

  /** An enumeration of types of enrollment in the Department of Continuing Education. */
  @AllArgsConstructor
  public enum Type {
    ANY("Any"),
    HIGH_SCHOOL("High School"),
    PERSONAL_ENRICHMENT("Personal Enrichment"),
    UNIVERSITY("University"),
    GENERAL("General");

    private final String name;

    @Override
    public String toString() {
      return name;
    }
  }
}
