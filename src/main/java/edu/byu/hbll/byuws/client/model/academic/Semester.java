package edu.byu.hbll.byuws.client.model.academic;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import edu.byu.hbll.byuws.client.model.academic.Semester.Deserializer;
import edu.byu.hbll.misc.Strings;
import java.io.IOException;
import java.time.Year;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Value object representing an academic semester or term at BYU.
 *
 * <p>This object is immutable and thread-safe.
 */
@JsonDeserialize(using = Deserializer.class)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
public final class Semester {

  private static final int CHARS_IN_YEAR = 4;

  @EqualsAndHashCode.Include private final Term term;

  @EqualsAndHashCode.Include private final Year year;

  private final String code;

  private final String description;

  /**
   * Capitalizes the first word in a given string.
   *
   * <p>If the given string is {@code null} or blank, then it will simply be returned as is. If the
   * first character of the string is not capitalizable (e.g.: a number), then the value of the
   * string will not change, but no guarantee is made to the primitive equality (==) of the strings.
   *
   * <p>For other capitalization options, see WordUtils in the Apache Commons Text library
   * (https://commons.apache.org/proper/commons-text/).
   *
   * @param str the string for which to capitalize the first word
   * @return the resulting string
   */
  private static String capitalizeFirst(String str) {
    return Strings.isBlank(str) ? str : Character.toTitleCase(str.charAt(0)) + str.substring(1);
  }

  /**
   * Constructs a new instance.
   *
   * @param term the term (Winter, Spring, Summer, Fall) of this semester within the academic
   *     calendar
   * @param year the calendar year of this semester
   */
  public Semester(Term term, Year year) {
    this.term = Objects.requireNonNull(term);
    this.year = Objects.requireNonNull(year);
    this.code = year.toString() + term.getCode();
    this.description = capitalizeFirst(term.toString().toLowerCase() + " " + year);
  }

  /**
   * Constructs and returns a new Semester using the canonical description for the term and year.
   *
   * @param description A string containing the (case-insensitive) name for the term (Winter,
   *     Spring, Summer, Fall) and a 4-digit year. The term name and year should be separated by a
   *     single space (ex.: "Winter 2001").
   * @return The corresponding semester object
   * @throws IllegalArgumentException If the provided description cannot be parsed into a Semester.
   */
  public static Semester fromDescription(String description) throws IllegalArgumentException {
    try {
      int breakpoint = description.indexOf(' ');
      Term term = Term.valueOf(description.substring(0, breakpoint).toUpperCase());
      Year year = Year.parse(description.substring(breakpoint + 1));

      return new Semester(term, year);
    } catch (StringIndexOutOfBoundsException | DateTimeParseException e) {
      throw new IllegalArgumentException(e);
    }
  }

  /**
   * Creates a new Semester using the canonical code for the term and year.
   *
   * @param code the canonical code for the semester (4-digit year followed by a single-digit term
   *     code; ex.: "20001")
   * @return The corresponding semester object
   * @throws IllegalArgumentException If the provided code cannot be parsed into a Semester.
   */
  public static Semester fromCode(String code) throws IllegalArgumentException {
    try {
      Year year = Year.parse(code.substring(0, CHARS_IN_YEAR));
      int termCode = Integer.valueOf(code.substring(CHARS_IN_YEAR));

      return new Semester(Term.fromCode(termCode), year);
    } catch (DateTimeParseException e) {
      throw new IllegalArgumentException(e);
    }
  }

  /** Enumeration of the possible terms supported by BYU's academic calendar. */
  @AllArgsConstructor
  @Getter
  public enum Term {
    WINTER(1),
    SPRING(3),
    SUMMER(4),
    FALL(5);

    private final int code;

    /**
     * Returns the enum value corresponding to the given code.
     *
     * @param code the code corresponding with the desired enum value
     * @return the appropriate value
     * @throws IllegalArgumentException if the given code does not correspond with any {@link Term}
     *     enum value
     */
    public static Term fromCode(int code) {
      return Stream.of(Term.values())
          .filter(term -> term.code == code)
          .findAny()
          .orElseThrow(() -> new IllegalArgumentException("Matching value not found"));
    }
  }

  /** Jackson deserializer that parses the semester returned by the control dates service. */
  public static class Deserializer extends JsonDeserializer<Semester> {

    @Override
    public Semester deserialize(JsonParser parser, DeserializationContext context)
        throws IOException, JsonProcessingException {
      return Semester.fromCode(parser.readValueAs(String.class));
    }
  }
}
