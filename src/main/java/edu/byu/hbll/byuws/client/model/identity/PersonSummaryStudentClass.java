package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
@Deprecated
public class PersonSummaryStudentClass {

  /**
   * The 2 to 5 character name of a discipline that has been adopted by the University.
   *
   * @return teaching area
   */
  @JsonAlias("teaching_area")
  private String teachingArea;

  /**
   * The individual primarily responsible for class instruction. Each class should have a Primary
   * Instructor.
   *
   * @return instructor
   */
  @JsonAlias("instructor")
  private String instructor;

  /**
   * Combination of catalog number, catalog suffix, and section number. Along with teaching area,
   * this displays the specific section of a class.
   *
   * @return catalog entry
   */
  @JsonAlias("catalog_entry")
  private String catalogEntry;
}
