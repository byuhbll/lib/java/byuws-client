package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
@Deprecated
public class PersonNames {

  @JsonAlias("person_summary_line")
  private PersonSummaryLine personSummaryLine;

  @JsonAlias("official_name")
  private PersonNamesOfficialName officialName;

  @JsonAlias("preferred_name")
  private PersonNamesPreferredName preferredName;
}
