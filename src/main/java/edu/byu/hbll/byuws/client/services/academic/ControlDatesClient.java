package edu.byu.hbll.byuws.client.services.academic;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.byuws.client.ByuwsClient;
import edu.byu.hbll.byuws.client.ByuwsClientException;
import edu.byu.hbll.byuws.client.model.academic.ControlDates;
import edu.byu.hbll.byuws.client.model.academic.ControlDates.Type;
import edu.byu.hbll.byuws.client.model.academic.Semester;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.misc.Streams;
import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

/** Client for making requests to the BYU control dates service. */
@AllArgsConstructor
public class ControlDatesClient {

  private static final UncheckedObjectMapper OBJECT_MAPPER = ObjectMapperFactory.newUnchecked();

  private static final String SCOPE = "scope";
  private static final String DATE_TYPES = "date_types";
  private static final String AS_OF = "as_of";
  private static final String SERVICE_PATH =
      String.format(
          "domains/legacy/academic/controls/controldatesws/v1/{%s}/{%s}/{%s}",
          SCOPE, AS_OF, DATE_TYPES);
  private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");

  @NonNull private final ByuwsClient client;

  /**
   * Retrieves the specified control dates for the current date.
   *
   * @param types the types of control dates to return
   * @return the retrieved control dates
   * @throws ByuwsClientException if the service fails
   */
  public List<ControlDates> currentControlDates(Type... types) throws ByuwsClientException {
    return controlDates(LocalDate.now(client.getProviderTimezone()), types);
  }

  /**
   * Retrieves the specified control dates for the given date.
   *
   * @param date the date for which to retrieve control dates
   * @param types the types of control dates to return
   * @return the retrieved control dates
   * @throws ByuwsClientException if the web service fails
   */
  public List<ControlDates> controlDates(LocalDate date, Type... types)
      throws ByuwsClientException {

    return invoke("asofdate", dateFormatter.format(date), types);
  }

  /**
   * Retrieves the specified control dates for the given {@link Semester}.
   *
   * @param semester the {@link Semester} for which to retrieve control dates
   * @param types the types of control dates to return
   * @return the retrieved control dates
   * @throws ByuwsClientException if the web service fails
   */
  public List<ControlDates> controlDates(Semester semester, Type... types)
      throws ByuwsClientException {

    return invoke("yearterm", semester.getCode(), types);
  }

  /**
   * Invokes the control dates service and parses the response as a list of {@link ControlDates}
   * objects.
   *
   * @param scope the scope of the control dates request
   * @param asOf the date or semester (will depend on scope) for which to retrieve control dates
   * @param types the list of control date types being requested
   * @return the response, parsed as a list of {@link ControlDates}
   * @throws ByuwsClientException if the web service fails
   */
  private List<ControlDates> invoke(String scope, String asOf, Type... types)
      throws ByuwsClientException {

    String joinedTypes = Stream.of(types).map(Object::toString).collect(Collectors.joining(","));
    URI endpoint =
        UriComponentsBuilder.fromUriString(SERVICE_PATH)
            .uriVariables(Map.of(SCOPE, scope, AS_OF, asOf, DATE_TYPES, joinedTypes))
            .build()
            .toUri();

    JsonNode response = client.exchange(endpoint, HttpMethod.GET, null, JsonNode.class);
    JsonNode dates = response.path("ControlDatesWSService").path("response").path("date_list");
    return Streams.of(dates)
        .map(node -> OBJECT_MAPPER.treeToValue(node, ControlDates.class))
        .collect(Collectors.toList());
  }
}
