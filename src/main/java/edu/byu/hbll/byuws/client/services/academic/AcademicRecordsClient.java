package edu.byu.hbll.byuws.client.services.academic;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.byuws.client.ByuwsClient;
import edu.byu.hbll.byuws.client.ByuwsClientException;
import java.net.URI;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

/** Client for making requests to the BYU academic records service. */
@AllArgsConstructor
public class AcademicRecordsClient {

  private static final String PATHPARAM_IDENTIFIER = "identifier";
  private static final String SERVICE_PATH =
      "domains/legacy/academic/records/publicinfo/v1/" + "{" + PATHPARAM_IDENTIFIER + "}";

  @NonNull private final ByuwsClient client;

  /**
   * Retrieves the academic records for the given person.
   *
   * @param identifier the "Person ID" or "Net ID" of the person for which to retrieve records
   * @return the academic records
   * @throws ByuwsClientException if the service fails
   */
  public JsonNode getRecords(String identifier) throws ByuwsClientException {
    URI endpoint =
        UriComponentsBuilder.fromUriString(SERVICE_PATH)
            .uriVariables(Map.of(PATHPARAM_IDENTIFIER, identifier))
            .build()
            .toUri();

    HttpHeaders headers = new HttpHeaders();
    headers.set("Acting-for", identifier);
    JsonNode response =
        client.exchange(endpoint, HttpMethod.GET, new HttpEntity<>(headers), JsonNode.class);
    return response.path("RecStdInfoService").path("response");
  }
}
