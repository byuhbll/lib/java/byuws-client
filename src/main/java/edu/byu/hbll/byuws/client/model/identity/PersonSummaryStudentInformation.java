package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import edu.byu.hbll.byuws.client.model.academic.Semester;
import java.util.List;
import lombok.Getter;

@Getter
@Deprecated
public class PersonSummaryStudentInformation {

  /**
   * List of classes for which this person is enrolled.
   *
   * @return classes
   */
  @JsonAlias("classes")
  private List<PersonSummaryStudentClass> classes;

  /**
   * Numerical designation of calendar year and semester/term. Format is yyyyt where t is a single
   * digit of 1 = Winter, 3 = Spring, 4 = Summer, 5 = Fall.
   *
   * @return year term
   */
  @JsonAlias("year_term")
  private Semester yearTerm;

  /**
   * Current course load measured in credit hours. One hour of credit generally includes a minimum
   * of one hour of instruction per week in a semester or two hours of instruction per week in a
   * term. A student is only awarded credit upon successful completion of the course.
   *
   * @return credit hours
   */
  @JsonAlias("credit_hours")
  private String creditHours;

  /**
   * Classification of students regarding how many credits they have earned toward their
   * undergraduate degree or which graduate-level program they are enrolled in. BYU has designated
   * this as directory information that it may disclose to the public without the student's written
   * consent. (The classification, not the credit hours a student has, is "directory.") For
   * individuals who have restricted their records, see Restricted Person.
   *
   * @return student role
   */
  @JsonAlias("student_role")
  private String studentRole;
}
