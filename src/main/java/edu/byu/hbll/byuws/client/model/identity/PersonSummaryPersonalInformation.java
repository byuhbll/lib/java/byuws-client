package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import java.time.LocalDate;
import lombok.Getter;

@Getter
@Deprecated
public class PersonSummaryPersonalInformation {

  /**
   * The length of time a person has lived, a calculated value based on date of birth and today's
   * date.
   *
   * @return age
   */
  @JsonAlias("age")
  private int age;

  /**
   * The country of which a person is a citizen.
   *
   * @return name of country of citizenship
   */
  @JsonAlias("citizenship")
  private String citizenship;

  /**
   * The country code associated with the country of which a person is a citizen.Note: Although
   * "Citizenship Country Code" is marked public, the citizenship country codes of restricted
   * persons are not. For more information, see Restricted Person.
   *
   * @return country of citizenship code
   */
  @JsonAlias("citizenship_code")
  private String citizenshipCode;

  /**
   * The combined day, month, and year of a person's birth, in yyyy-mm-dd format (e.g. 1991-06-04).
   *
   * @return date of birth
   */
  @JsonAlias("date_of_birth")
  private LocalDate dateOfBirth;

  /**
   * Flag indicating whether the person is deceased. Note: Although "Deceased" is marked public, it
   * is not public for restricted persons. For more information, see Restricted Person.
   *
   * @return deceased
   */
  @JsonAlias("deceased")
  private boolean deceased;

  /**
   * The name(s) of one or more social groups, including race, each having a common national or
   * cultural tradition, which a person reports as identifying with. Ethnicity may be represented by
   * multiple values per person.
   *
   * @return ethnicity
   */
  @JsonAlias("ethnicity")
  private String ethnicity;

  /**
   * Returns a three character representation of ethnicity.
   *
   * @return ethnicity code
   */
  @JsonAlias("ethnicity_code")
  private String ethnicityCode;

  /**
   * Returns a person's sex. Values include Male, Female and Unknown. See also Sex Code. Legacy
   * systems may have used the term gender. Note: Although "Sex" is marked public, the sexes
   * associated with restricted persons are not. For more information, see Restricted Person.
   *
   * @return gender
   */
  @JsonAlias("gender")
  private String gender;

  /**
   * Returns a one character representation of a person's sex. Values include M, F and ?. See also
   * Sex. Legacy systems may have used the term gender.
   *
   * @return gender code
   */
  @JsonAlias("gender_code")
  private String genderCode;

  /**
   * The combined Home Country, Home State/Province, and Home City of a person. May be used in
   * conjunction with other information to help accurately identify a person.
   *
   * @return home town
   */
  @JsonAlias("home_town")
  private String homeTown;

  /**
   * The marital status of a person.
   *
   * @return marital status
   */
  @JsonAlias("marital_status")
  private String maritalStatus;

  /**
   * Code that indicates the marital status of a person.
   *
   * @return marital status code
   */
  @JsonAlias("marital_status_code")
  private String maritalStatusCode;

  /**
   * Religion of the given person. Includes two values: Religion Name and Religion Code.
   *
   * @return religion
   */
  @JsonAlias("religion")
  private String religion;

  /**
   * The three-digit alphabetical code associated with the religion of the given person.
   *
   * @return religion code
   */
  @JsonAlias("religion_code")
  private String religionCode;

  /**
   * Flag indicating whether a person has requested anonymity in the university's Identity system.
   * The Restricted Flag carries more serious implications than the Unlisted Data Flag: if a person
   * is marked as "restricted," his or her presence at the University MUST NOT be made known to the
   * public.
   *
   * @return is restricted record
   */
  @JsonAlias("restricted_record")
  private boolean restrictedRecord;
}
