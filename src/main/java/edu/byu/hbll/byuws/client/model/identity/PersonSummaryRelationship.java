package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
@Deprecated
public class PersonSummaryRelationship {

  /**
   * Classification of students regarding how many credits they have earned toward their
   * undergraduate degree or which graduate-level program they are enrolled in. BYU has designated
   * this as directory information that it may disclose to the public without the student's written
   * consent. (The classification, not the credit hours a student has, is "directory.") For
   * individuals who have restricted their records, see Restricted Person.
   *
   * @return student classification
   */
  @JsonAlias("student")
  private String student;

  /**
   * The 9-digit, unique, internal OIT identifier used by some legacy systems. The Person ID is not
   * to be used at points of service. Any system created in 2016 or after should use BYU ID instead
   * of Person ID for system-to-system identification.
   *
   * @return person ID
   */
  @JsonAlias("person_id")
  private String personId;

  /**
   * The complete name of the person, presented in the following order: First Name, Middle Name,
   * Surname (commas not included).
   *
   * @return the name
   */
  @JsonAlias("name")
  private String name;

  /**
   * Description of the nature of the affiliation between the university and a peripheral member of
   * the university community.
   *
   * @return affiliation
   */
  @JsonAlias("affiliation")
  private String affiliation;

  /**
   * Flag indicating whether the person is deceased. Note: Although "Deceased" is marked public, it
   * is not public for restricted persons. For more information, see Restricted Person.
   *
   * @return is deceased
   */
  @JsonAlias("deceased")
  private boolean deceased;

  /**
   * Designation of whether an employee is paid a salary or hourly. The two values are Salary or
   * Hourly.
   *
   * @return employee
   */
  @JsonAlias("employee")
  private String employee;

  /**
   * Returns relationship type.
   *
   * @return relationship type
   */
  @JsonAlias("type")
  private String type;
}
