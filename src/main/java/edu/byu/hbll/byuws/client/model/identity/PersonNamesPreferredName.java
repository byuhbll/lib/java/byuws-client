package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
@Deprecated
public class PersonNamesPreferredName {

  @JsonAlias("preferred_first_name")
  private String preferredFirstName;

  @JsonAlias("use_preferred_name_on_id_card")
  private boolean usePreferredNameOnIdCard;
}
