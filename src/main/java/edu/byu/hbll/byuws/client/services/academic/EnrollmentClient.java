package edu.byu.hbll.byuws.client.services.academic;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.byuws.client.ByuwsClient;
import edu.byu.hbll.byuws.client.ByuwsClientException;
import edu.byu.hbll.byuws.client.model.academic.Enrollment;
import edu.byu.hbll.byuws.client.model.academic.Semester;
import edu.byu.hbll.misc.Strings;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

/** Client for making requests to the BYU enrollment service. */
@AllArgsConstructor
public class EnrollmentClient {

  private static final String ID_TYPE = "idType";
  private static final String IDENTIFIER = "identifier";
  private static final String YEAR_TERM = "yearTerm";
  private static final String SERVICE_PATH =
      String.format(
          "domains/legacy/academic/registration/enrollment/v1/bothschedules/{%s}/{%s}",
          ID_TYPE, IDENTIFIER);

  private static final String PERSON_ID = "personid";
  private static final String BYU_ID = "byuid";
  private static final String NET_ID = "netid";

  private final ByuwsClient client;

  /**
   * Retrieves the course schedule for the given person and semester(s).
   *
   * @param personId the OIT Person ID of the person for which to retrieve enrollment data
   * @param semesters the semesters for which to retrieve enrollment data
   * @return the retrieved enrollment data
   * @throws ByuwsClientException if the service fails
   */
  public Enrollment courseScheduleForPersonId(String personId, Semester... semesters)
      throws ByuwsClientException {
    return invoke(PERSON_ID, personId, semesters);
  }

  /**
   * Retrieves the course schedule for the given person and semester(s).
   *
   * @param byuId the BYU ID number of the person for which to retrieve enrollment data
   * @param semesters the semesters for which to retrieve enrollment data
   * @return the retrieved enrollment data
   * @throws ByuwsClientException if the service fails
   */
  public Enrollment courseScheduleForByuId(String byuId, Semester... semesters)
      throws ByuwsClientException {
    return invoke(BYU_ID, byuId, semesters);
  }

  /**
   * Retrieves the course schedule for the given person and semester(s).
   *
   * @param netId the Net ID of the person for which to retrieve enrollment data
   * @param semesters the semesters for which to retrieve enrollment data
   * @return the retrieved enrollment data
   * @throws ByuwsClientException if the service fails
   */
  public Enrollment courseScheduleForNetId(String netId, Semester... semesters)
      throws ByuwsClientException {
    return invoke(NET_ID, netId, semesters);
  }

  /**
   * Retrieves the course schedule for the given person and semester(s).
   *
   * @param idType the type of identifier used to identify this person
   * @param identifier the identifier of the person for which to retrieve enrollment data
   * @param semesters the semesters for which to retrieve enrollment data
   * @return the retrieved enrollment data
   * @throws ByuwsClientException if the service fails
   */
  private Enrollment invoke(String idType, String identifier, Semester... semesters)
      throws ByuwsClientException {

    // Validate args.
    Strings.requireNonBlank(identifier, "person identifier cannot be blank");
    if (semesters.length < 1) {
      throw new IllegalArgumentException("at least one semester must be specified");
    }

    // Build the endpoint.
    UriComponentsBuilder endpoint =
        UriComponentsBuilder.fromUriString(SERVICE_PATH)
            .uriVariables(Map.of(ID_TYPE, idType))
            .uriVariables(Map.of(IDENTIFIER, identifier))
            .queryParam("numYearTerms", semesters.length);
    for (Semester semester : semesters) {
      endpoint.queryParam(YEAR_TERM, semester.getCode());
    }

    // Invoke the service and parse the response.
    JsonNode response =
        client.exchange(endpoint.build().toUri(), HttpMethod.GET, null, JsonNode.class);
    return ByuwsClient.OBJECT_MAPPER.treeToValue(
        response.path("EnrollmentService").path("response"), Enrollment.class);
  }
}
