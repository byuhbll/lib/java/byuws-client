package edu.byu.hbll.byuws.client.model.academic;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import edu.byu.hbll.byuws.client.json.FlagDeserializer;
import lombok.Getter;

@Getter
public class EnrollmentInstructorClass {

  @JsonAlias("cross_listed")
  @JsonDeserialize(using = FlagDeserializer.class)
  private boolean crossListed;

  @JsonAlias("year_term")
  @JsonDeserialize(using = Semester.Deserializer.class)
  private Semester semester;

  @JsonAlias("subject_area")
  private String subjectArea;

  @JsonAlias("catalog_number")
  private String catalogNumber;

  @JsonAlias("section_type")
  private String sectionType;

  @JsonAlias("curriculum_id")
  private String curriculumId;

  @JsonAlias("title_code")
  private String titleCode;

  @JsonAlias("section_number")
  private String sectionNumber;

  @JsonAlias("block_code")
  private String blockCode;

  @JsonAlias("instructor_type")
  private String instructorType;

  @JsonAlias("course_title")
  private String courseTitle;

  @JsonAlias("section_size")
  private String sectionSize;

  @JsonAlias("total_enrolled")
  private String totalEnrolled;
}
