package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
@Deprecated
public class PersonNamesOfficialName {

  @JsonAlias("sort_name")
  private String sortName;

  @JsonAlias("surname")
  private String surname;

  @JsonAlias("rest_of_name")
  private String restOfName;

  @JsonAlias("first_name")
  private String firstName;

  @JsonAlias("middle_name")
  private String middleName;

  @JsonAlias("surname_position")
  private String surnamePosition;
}
