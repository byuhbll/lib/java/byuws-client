package edu.byu.hbll.byuws.client;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import edu.byu.hbll.byuws.client.ByuwsClient.Deserializer;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Base client used for preparing and invoking requests to the BYU web services.
 *
 * <p>This class is thread-safe.
 */
@JsonDeserialize(using = Deserializer.class)
public class ByuwsClient {

  /** The default baseUri to the BYU web services. */
  public static final URI DEFAULT_BASE_URI = URI.create("https://api.byu.edu/");

  /** The default timezone of the BYU web services provider. */
  public static final ZoneId DEFAULT_PROVIDER_ZONE_ID = ZoneId.of("America/Denver");

  /** The {@link ObjectMapper} used throughout this library. THIS OBJECT MUST NOT BE MODIFIED. */
  public static final UncheckedObjectMapper OBJECT_MAPPER = ObjectMapperFactory.newUnchecked();

  /** The underlying REST template that will be used to invoke requests. */
  private static final RestTemplate client = new RestTemplate();

  /** The endpoint (relative to {@link #baseUri}) where the login service is hosted. */
  private static final URI LOGIN_ENDPOINT = URI.create("token");

  /** The default number of attempts allowed; this can be overridden using the builder. */
  private static final int DEFAULT_ATTEMPTS_ALLOWED = 3;

  private final URI baseUri;
  private final ZoneId providerTimezone;
  private final HttpEntity<Object> loginEntity;

  private String accessToken;
  private Instant expiration = Instant.EPOCH;
  private int attemptsAllowed;

  /**
   * Constructs and returns a new builder for this class.
   *
   * @param clientId the clientId to use for authentication
   * @param clientSecret the clientSecret to use for authentication
   * @return the builder
   */
  public static Builder builder(String clientId, String clientSecret) {
    return new Builder(clientId, clientSecret);
  }

  /**
   * Constructs a new instance using the provided builder as a template.
   *
   * @param builder the builder to use as a template
   */
  private ByuwsClient(Builder builder) {
    this.baseUri = builder.baseUri;
    this.attemptsAllowed = builder.attemptsAllowed;
    this.providerTimezone = builder.providerTimezone;

    // The HTTP headers needed for the login operation are constant across the client's lifetime.
    HttpHeaders headers = new HttpHeaders();
    headers.setBasicAuth(builder.clientId, builder.clientSecret);
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    this.loginEntity = new HttpEntity<>("grant_type=client_credentials", headers);
  }

  /**
   * Creates a copy of this instance that replaces the authentication credentials; currently used
   * only for testing purposes.
   *
   * @param clientId the new clientId to use for authentication
   * @param clientSecret the new clientSecret to use for authentication
   * @return the new client
   */
  ByuwsClient withCredentials(String clientId, String clientSecret) {
    return ByuwsClient.builder(clientId, clientSecret)
        .providerTimezone(providerTimezone)
        .baseUri(baseUri)
        .attemptsAllowed(attemptsAllowed)
        .build();
  }

  /**
   * Returns the current OAuth token if one is set and has not expired; otherwise, invokes the login
   * service with the configured clientId and clientSecret to create a new session.
   *
   * @return this client, updated with the OAUTH credentials
   * @throws ByuwsClientException if the login request fails
   */
  public synchronized ByuwsClient login() throws ByuwsClientException {
    // If our current session has not yet expired, reuse it without making any web service calls.
    if (Instant.now().isBefore(expiration)) {
      return this;
    }

    JsonNode response = exchange(LOGIN_ENDPOINT, HttpMethod.POST, loginEntity, JsonNode.class);
    accessToken = response.path("access_token").asText(null);
    int expirationSeconds = response.path("expiration").asInt(0);
    expiration = Instant.now().plusSeconds(expirationSeconds - 1);
    return this;
  }

  /**
   * Copies the given {@link HttpEntity} (or creates one if {@code null} is provided) and modifies
   * it to include a valid OAuth token. This method may invoke the login service if no token has
   * been assigned or if the token is about to expire.
   *
   * @param entity the entity (headers and/or body) to write to the request (may be null)
   * @return the updated copy of the original entity
   * @throws ByuwsClientException if the login request fails
   */
  private HttpEntity<?> authorize(HttpEntity<?> entity) throws ByuwsClientException {
    // Ensure that we have an active session.
    login();

    // Capture the request body and headers from the provided entity (or set sane defaults if no
    // entity was provided); then ensure that the authorization token has been set.
    Object requestBody = entity != null ? entity.getBody() : null;
    HttpHeaders headers =
        entity != null ? HttpHeaders.writableHttpHeaders(entity.getHeaders()) : new HttpHeaders();
    headers.setBearerAuth(accessToken);

    // Return the new entity with the authorization header set.
    return new HttpEntity<>(requestBody, headers);
  }

  /**
   * Invokes the given request and returns the response.
   *
   * <p>Unless this is a login request (which is sent as-is), this method will copy the given {@link
   * HttpEntity} (or create one if {@code null} is provided) and modify it to include a valid OAuth
   * token. This authorization step may invoke the login service if no token has been assigned or if
   * the token is about to expire.
   *
   * @param endpoint The uri (relative to the {@link #baseUri} configured at construction time)
   * @param method the HTTP method (GET, POST, etc)
   * @param entity the entity (headers and/or body) to write to the request (may be null)
   * @param responseType the type of the return value
   * @param <T> the type of the return value
   * @return the response as a {@link ResponseEntity}
   * @throws ByuwsClientException if the request fails
   */
  public <T> T exchange(
      URI endpoint, HttpMethod method, HttpEntity<?> entity, Class<T> responseType)
      throws ByuwsClientException {

    // Resolve the URL in preparation for invocation.
    URI uri = baseUri.resolve(endpoint);

    // Make sure to only allow retries for GET methods and the login service.
    boolean areRetriesAllowed = method.equals(HttpMethod.GET) || endpoint.equals(LOGIN_ENDPOINT);
    int adjustedAttemptsAllowed = areRetriesAllowed ? attemptsAllowed : 1;

    // Guarantee that we have an active session unless this is a login attempt (trying to authorize
    // a login attempt would put us in an infinite recursive state.
    entity = endpoint.equals(LOGIN_ENDPOINT) ? entity : authorize(entity);

    // Invoke the service; if it fails, try again (if allowed) until we run out of retry attempts.
    ByuwsClientException exception = null;
    for (int i = 0; i < adjustedAttemptsAllowed; i++) {
      try {
        try {
          return client.exchange(uri, method, entity, responseType).getBody();
        } catch (RestClientException e) {
          throw new ByuwsClientException(method, uri, e);
        }
      } catch (ByuwsClientException e) {
        exception = e;
      }
    }
    // If we did not returned with a successful login by the end of the loop, throw an exception.
    throw exception;
  }

  /**
   * Some legacy web services return JSON in the ISO-8859-1 encoding; use this invocation instead of
   * {@link #exchange(URI, HttpMethod, HttpEntity, Class)} when interacting with those services.
   *
   * <p>While the JSON specification mandates that UTF-8 is the only valid character encoding for
   * JSON (https://tools.ietf.org/html/rfc8259#section-8.1), some legacy services do not follow this
   * rule. As a result, any response which includes Unicode characters will result in errors when
   * the normal invocation rules are used. OIT has indicated that these services will NOT be fixed,
   * so this method offers a workaround for those services at a minor performance cost.
   *
   * <p>Note that we have been unable to replicate a true ISO-8859-1 response when using Hoverfly
   * recordings, so we do not have any automated tests for this method. However, we have tested this
   * solution against a real-life user and found that it works.
   *
   * @param endpoint The uri (relative to the {@link #baseUri} configured at construction time)
   * @param method the HTTP method (GET, POST, etc)
   * @param entity the entity (headers and/or body) to write to the request (may be null)
   * @param responseType the type of the return value
   * @param <T> the type of the return value
   * @return the response as a {@link ResponseEntity}
   * @throws ByuwsClientException if the request fails
   */
  public <T> T exchangeIso8859Json(
      URI endpoint, HttpMethod method, HttpEntity<?> entity, Class<T> responseType) {
    byte[] rawResponse = exchange(endpoint, method, entity, byte[].class);
    String normalizedResponse = new String(rawResponse, StandardCharsets.ISO_8859_1);
    return OBJECT_MAPPER.readValue(normalizedResponse, responseType);
  }

  /**
   * Returns the providerTimezone for this client.
   *
   * @return the providerTimezone
   */
  public ZoneId getProviderTimezone() {
    return providerTimezone;
  }

  /**
   * Returns the accessToken for this client; used for testing.
   *
   * @return the accessToken
   */
  String getAccessToken() {
    return accessToken;
  }

  /** Builder for {@link ByuwsClient}. */
  public static class Builder {

    private String clientId;
    private String clientSecret;
    private URI baseUri = DEFAULT_BASE_URI;
    private ZoneId providerTimezone = DEFAULT_PROVIDER_ZONE_ID;
    private int attemptsAllowed = DEFAULT_ATTEMPTS_ALLOWED;

    /**
     * Constructs a new instance.
     *
     * @param clientId the BASIC user to use to retrieve an OAUTH token
     * @param clientSecret the BASIC password to use to retrieve an OAUTH token
     */
    private Builder(String clientId, String clientSecret) {
      this.clientId = Objects.requireNonNull(clientId, "clientId cannot be null");
      this.clientSecret = Objects.requireNonNull(clientSecret, "clientSecret cannot be null");
    }

    /**
     * The maximum number of attempts allowed for login requests or generic GET requests. The
     * default value is {@code 3}; set to {@code 1} to disable retries.
     *
     * <p>For safety reasons, retries are presently disallowed for write requests and this value
     * will be ignored for non-GET requests (except login).
     *
     * @param attemptsAllowed the number of allowed attempts (must be greater than 0)
     * @return this builder
     */
    public Builder attemptsAllowed(int attemptsAllowed) {
      if (attemptsAllowed < 1) {
        throw new IllegalArgumentException("attemptsAllowed must be greater than 0");
      }
      this.attemptsAllowed = attemptsAllowed;
      return this;
    }

    /**
     * Defines the baseUri where the web services are hosted.
     *
     * @param baseUri the baseUri where the web services are hosted
     * @return this builder
     */
    public Builder baseUri(URI baseUri) {
      Objects.requireNonNull(baseUri, "baseUri cannot be null");
      // Guarantee that the base path ends with a trailing slash (so that endpoint resolution works
      // as expected).
      if (!baseUri.getPath().endsWith("/")) {
        baseUri = URI.create(baseUri.toString() + "/");
      }
      this.baseUri = baseUri;
      return this;
    }

    /**
     * Defines the timezone where the web services are hosted.
     *
     * @param providerTimezone the timezone where the web services are hosted
     * @return this builder
     */
    public Builder providerTimezone(ZoneId providerTimezone) {
      this.providerTimezone =
          Objects.requireNonNull(providerTimezone, "providerTimezone cannot be null");
      return this;
    }

    /**
     * Builds a new {@link ByuwsClient} using this {@link Builder} as a template.
     *
     * @return the constructed {@link ByuwsClient}
     */
    public ByuwsClient build() {
      return new ByuwsClient(this);
    }
  }

  public static class Deserializer extends JsonDeserializer<ByuwsClient> {

    @Override
    public ByuwsClient deserialize(JsonParser parser, DeserializationContext context)
        throws IOException, JsonProcessingException {
      JsonNode json = parser.readValueAsTree();

      ByuwsClient.Builder builder =
          new ByuwsClient.Builder(
              json.path("clientId").asText(null), json.path("clientSecret").asText(null));
      Optional.ofNullable(json.path("baseUri").asText(null))
          .map(URI::create)
          .ifPresent(builder::baseUri);
      Optional.ofNullable(json.path("providerTimezone").asText(null))
          .map(ZoneId::of)
          .ifPresent(builder::providerTimezone);

      return builder.build();
    }
  }
}
