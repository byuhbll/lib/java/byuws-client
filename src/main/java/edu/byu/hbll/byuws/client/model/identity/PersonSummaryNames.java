package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
@Deprecated
public class PersonSummaryNames {

  /**
   * The complete name of the person, presented in the following order: Surname, First Name, Middle
   * Name (commas not included). The term "Sort Name" is a synonym for this.
   *
   * @return complete name
   */
  @JsonAlias("complete_name")
  private String completeName;

  /**
   * The person's preferred name, derived from Preferred First Name and Preferred Surname.
   *
   * @return preferred name
   */
  @JsonAlias("preferred_name")
  private String perferredName;
}
