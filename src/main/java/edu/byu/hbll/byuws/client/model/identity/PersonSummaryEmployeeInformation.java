package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
@Deprecated
public class PersonSummaryEmployeeInformation {

  /**
   * More information about this person's current employment (contrary to the name of the field,
   * this object contains more information than just the date they were hired).
   *
   * @return hiring information
   */
  @JsonAlias("date_hired")
  private PersonSummaryDateHired dateHired;

  /**
   * An organizational grouping of financial Operating Units; a Department usually contains one or
   * more Operating Units and each Department may contain Departments as part of a hierarchical
   * reporting structure within the University.
   *
   * @return department
   */
  @JsonAlias("department")
  private String department;

  /**
   * The combination of Employee Classification Code, Employee Status Code and Employee Standing
   * Code that are each separated by a forward slash.
   *
   * @return employee role
   */
  @JsonAlias("employee_role")
  private String employeeRole;

  /**
   * Returns a descriptive label of the Job Code.
   *
   * @return job title
   */
  @JsonAlias("job_title")
  private String jobTitle;

  /**
   * The Person ID of this employee's direct supervisor.
   *
   * <p>According to OIT, this 9-digit unique, internal OIT identifier is used by some legacy
   * systems. The Person ID is not to be used at points of service; any system created in 2016 or
   * after should use BYU ID instead of Person ID for system-to-system identification.
   *
   * @return supervisor's Person ID
   */
  @JsonAlias("reports_to_id")
  private String reportsToId;

  /**
   * The name (preferred first name + preferred surname) of this employee's direct supervisor.
   *
   * @return supervisor's name
   */
  @JsonAlias("reports_to_name")
  private String reportsToName;
}
