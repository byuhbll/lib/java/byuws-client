package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import java.time.LocalDate;
import lombok.Getter;

@Getter
@Deprecated
public class PersonSummaryDateHired {

  /**
   * Employee classification based on the expected amount of time a person is employed per pay
   * period. The most common employee statuses are Full-time, Part-time and On Call.
   *
   * @return qualification
   */
  @JsonAlias("qualification")
  private String qualification;

  /**
   * The number of years an employee has worked in a non-student role at BYU.
   *
   * @return years of service
   */
  @JsonAlias("years_of_service")
  private String yearsOfService;

  /**
   * The date on which an employee started their most recent employment at BYU.
   *
   * @return hire date
   */
  @JsonAlias("date")
  private LocalDate date;
}
