package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import edu.byu.hbll.byuws.client.json.FlagDeserializer;
import lombok.Getter;

/** Jackson bean representing the identifiers associated with a given person. */
@Getter
@Deprecated
public class Identifiers {

  @JsonAlias("person_id")
  private String personId;

  @JsonAlias("byu_id")
  private String byuId;

  @JsonAlias("net_id")
  private String netId;

  @JsonAlias("sort_name")
  private String sortName;

  @JsonAlias("authorized")
  @JsonDeserialize(using = FlagDeserializer.class)
  private boolean authorized;
}
