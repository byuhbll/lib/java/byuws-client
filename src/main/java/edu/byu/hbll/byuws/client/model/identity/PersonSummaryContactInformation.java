package edu.byu.hbll.byuws.client.model.identity;

import com.fasterxml.jackson.annotation.JsonAlias;
import java.util.List;
import lombok.Getter;

@Getter
@Deprecated
public class PersonSummaryContactInformation {

  @JsonAlias("email")
  private String email;

  @JsonAlias("email_address_unlisted")
  private boolean emailAddressUnlisted;

  @JsonAlias("email_address")
  private String emailAddress;

  @JsonAlias("mailing_address_unlisted")
  private boolean mailingAddressUnlisted;

  @JsonAlias("mailing_address")
  private List<String> mailingAddress;

  @JsonAlias("mailing_phone_unlisted")
  private boolean mailingPhoneUnlisted;

  @JsonAlias("mailing_phone")
  private String mailingPhone;

  @JsonAlias("phone_number")
  private String phoneNumber;

  @JsonAlias("work_address_unlisted")
  private boolean workAddressUnlisted;

  @JsonAlias("work_address")
  private List<String> workAddress;

  @JsonAlias("work_email_address")
  private String workEmailAddress;

  @JsonAlias("work_phone_unlisted")
  private boolean workPhoneUnlisted;

  @JsonAlias("work_phone")
  private String workPhone;
}
