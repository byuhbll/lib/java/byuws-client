package edu.byu.hbll.byuws.client.model.academic;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import edu.byu.hbll.byuws.client.json.FlagDeserializer;
import java.util.List;
import lombok.Getter;

@Getter
public class EnrollmentStudentClass {

  @JsonAlias("year_term")
  @JsonDeserialize(using = Semester.Deserializer.class)
  private Semester semester;

  @JsonAlias("subject_area")
  private String subjectArea;

  @JsonAlias("catalog_number")
  private String catalogNumber;

  @JsonAlias("section_number")
  private String sectionNumber;

  @JsonAlias("curriculum_id")
  private String curriculumId;

  @JsonAlias("title_code")
  private String titleCode;

  @JsonAlias("section_type")
  private String sectionType;

  @JsonAlias("audit")
  @JsonDeserialize(using = FlagDeserializer.class)
  private boolean audit;

  @JsonAlias("block_code")
  private String blockCode;

  @JsonAlias("credit_hours")
  private String creditHours;

  @JsonAlias("course_title")
  private String courseTitle;

  @JsonAlias("instructor_set")
  private List<EnrollmentInstructor> instructors;
}
