package edu.byu.hbll.byuws.client.model.academic;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
public class EnrollmentInstructor {

  @JsonAlias("instructor_type")
  private String type;

  @JsonAlias("instructor_name")
  private String name;
}
