package edu.byu.hbll.byuws.client.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;

public class FlagDeserializer extends JsonDeserializer<Boolean> {

  @Override
  public Boolean deserialize(JsonParser parser, DeserializationContext context)
      throws IOException, JsonProcessingException {
    return "Y".equals(parser.readValueAs(String.class));
  }
}
