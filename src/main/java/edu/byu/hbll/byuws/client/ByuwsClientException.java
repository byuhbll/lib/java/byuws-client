package edu.byu.hbll.byuws.client;

import java.net.URI;
import lombok.Getter;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;

@Getter
public class ByuwsClientException extends RestClientException {

  private static final int DEFAULT_ATTEMPTS = 1;

  private HttpMethod method;
  private URI uri;
  private int status;
  private String body = "";

  /**
   * Constructs a new instance.
   *
   * @param method the HTTP method which was invoked
   * @param uri the URL of the service that was executed
   * @param cause the underlying cause of the failure
   */
  public ByuwsClientException(HttpMethod method, URI uri, RestClientException cause) {
    this(method, uri, cause, DEFAULT_ATTEMPTS);
  }

  /**
   * Constructs a new instance.
   *
   * @param method the HTTP method which was invoked
   * @param uri the URL of the service that was executed
   * @param cause the underlying cause of the failure
   * @param attempts the number of attempts that were made before throwing the exception
   */
  public ByuwsClientException(HttpMethod method, URI uri, RestClientException cause, int attempts) {
    super(buildMessage(method, uri, cause, attempts), cause);
    this.method = method;
    this.uri = uri;
    if (cause instanceof RestClientResponseException) {
      RestClientResponseException exception = (RestClientResponseException) cause;
      this.status = exception.getRawStatusCode();
      this.body = exception.getResponseBodyAsString();
    }
  }

  /**
   * Constructs a new instance.
   *
   * @param method the HTTP method which was invoked
   * @param uri the URL of the service that was executed
   * @param status the HTTP status code returned by the service
   * @param body the response body returned by the service
   */
  public ByuwsClientException(HttpMethod method, URI uri, int status, String body) {
    super(buildMessage(method, uri, status, body, DEFAULT_ATTEMPTS));
    this.method = method;
    this.uri = uri;
    this.status = status;
    this.body = body;
  }

  /**
   * Builds a formatted message using the attributes sent to the constructor variant that uses an
   * underlying cause.
   *
   * @param method the HTTP method which was invoked
   * @param uri the URL of the service that was executed
   * @param cause the underlying cause of the failure
   * @param attempts the number of attempts that were made before throwing the exception
   */
  private static String buildMessage(
      HttpMethod method, URI uri, RestClientException cause, int attempts) {

    if (cause instanceof RestClientResponseException) {
      RestClientResponseException exception = (RestClientResponseException) cause;
      int status = exception.getRawStatusCode();
      String body = exception.getResponseBodyAsString();
      return buildMessage(method, uri, status, body, attempts);
    } else {
      return cause.getMessage();
    }
  }

  /**
   * Builds a formatted message using the attributes sent to the constructor variant that avoids
   * using an underlying cause.
   *
   * @param method the HTTP method which was invoked
   * @param uri the URL of the service that was executed
   * @param status the HTTP status code returned by the service
   * @param body the response body returned by the service
   * @param attempts the number of attempts that were made before throwing the exception
   */
  private static String buildMessage(
      HttpMethod method, URI uri, int status, String body, int attempts) {

    String retryMsg = attempts > DEFAULT_ATTEMPTS ? "After " + attempts + " attempts, " : "";
    return String.format(retryMsg + "%s %s returned %d: %s", method, uri, status, body);
  }
}
