package edu.byu.hbll.byuws.client.services.identity;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.byuws.client.ByuwsClient;
import edu.byu.hbll.byuws.client.ByuwsClientException;
import edu.byu.hbll.byuws.client.model.identity.Identifiers;
import java.net.URI;
import java.util.Objects;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

/** Client for making requests to the BYU resolve identity service. */
@Deprecated
public class ResolveIdentityClient {

  private static final String PERSON_ID = "person_id";
  private static final String BYU_ID = "byu_id";
  private static final String NET_ID = "net_id";
  private static final String SERVICE_PATH = "identity/person/resolveIdentity/v1/ids";

  private final ByuwsClient client;

  /**
   * Constructs a new instance.
   *
   * @param client the {@link ByuwsClient} to use to invoke the requests made using the client
   */
  public ResolveIdentityClient(ByuwsClient client) {
    this.client = Objects.requireNonNull(client);
  }

  /**
   * Resolves the given Person ID.
   *
   * @param personId the Person ID to resolve
   * @return the other identifiers associated with this person
   * @throws ByuwsClientException if the service fails
   */
  public Identifiers resolvePersonId(String personId) throws ByuwsClientException {
    return resolve(PERSON_ID, personId);
  }

  /**
   * Resolves the given BYU ID number.
   *
   * @param byuId the BYU ID number to resolve
   * @return the other identifiers associated with this person
   * @throws ByuwsClientException if the service fails
   */
  public Identifiers resolveByuId(String byuId) throws ByuwsClientException {
    return resolve(BYU_ID, byuId);
  }

  /**
   * Resolves the given Net ID.
   *
   * @param netId the Net ID to resolve
   * @return the other identifiers associated with this person
   * @throws ByuwsClientException if the service fails
   */
  public Identifiers resolveNetId(String netId) throws ByuwsClientException {
    return resolve(NET_ID, netId);
  }

  /**
   * Helper method which invokes the resolve identity service.
   *
   * @param identifierType the type of identifier to resolve
   * @param identifier the identifier to resolve
   * @return the other identifiers associated with this person
   * @throws ByuwsClientException if the service fails
   */
  private Identifiers resolve(String identifierType, String identifier)
      throws ByuwsClientException {

    URI endpoint =
        UriComponentsBuilder.fromUriString(SERVICE_PATH)
            .queryParam(identifierType, identifier)
            .build()
            .toUri();

    JsonNode response = client.exchange(endpoint, HttpMethod.GET, null, JsonNode.class);
    return ByuwsClient.OBJECT_MAPPER.treeToValue(
        response.path("ResolveIdentityService").path("response"), Identifiers.class);
  }
}
