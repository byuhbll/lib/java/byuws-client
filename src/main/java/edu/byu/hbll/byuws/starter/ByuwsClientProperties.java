package edu.byu.hbll.byuws.starter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("byuhbll.byuws-client")
@NoArgsConstructor
@AllArgsConstructor
public class ByuwsClientProperties {

  /** The Client ID to use when communicating with the BYU Web Services. */
  private String clientId;

  /** The Client Secret generated from the API store. */
  private String clientSecret;

  /** The Root URI where the BYU Web Services are located. */
  private String baseUri;

  /** The time zone ID. */
  private String zoneId;

  /** The number of attempts allowed for retrying failed requests. */
  private int attemptsAllowed;
}
