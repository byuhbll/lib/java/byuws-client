package edu.byu.hbll.byuws.starter;

import edu.byu.hbll.byuws.client.ByuwsClient;
import edu.byu.hbll.misc.Strings;
import java.net.URI;
import java.time.ZoneId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ByuwsClientProperties.class)
@AllArgsConstructor
@Data
@Slf4j
@ConditionalOnProperty(
    prefix = "byuhbll.byuws-client",
    name = {"client-id", "client-secret"})
public class ByuwsClientAutoConfiguration {

  private final ByuwsClientProperties byuwsClientProperties;

  /**
   * Constructs a ByuwsClient bean using the ByuwsClientProperties. Optional settings are checked
   * before adding them to the builder.
   *
   * <p>The clientId and clientSecret are required to construct the client.
   *
   * @return a constructed ByuwsClient
   */
  @Bean
  @ConditionalOnMissingBean
  public ByuwsClient byuwsClient() {
    log.info("Initializing ByuwsClient");

    ByuwsClient.Builder builder =
        ByuwsClient.builder(
            byuwsClientProperties.getClientId(), byuwsClientProperties.getClientSecret());

    if (!Strings.isBlank(byuwsClientProperties.getBaseUri())) {
      builder.baseUri(URI.create(byuwsClientProperties.getBaseUri()));
    }

    if (!Strings.isBlank(byuwsClientProperties.getZoneId())) {
      builder.providerTimezone(ZoneId.of(byuwsClientProperties.getZoneId()));
    }

    if (byuwsClientProperties.getAttemptsAllowed() > 0) {
      builder.attemptsAllowed(byuwsClientProperties.getAttemptsAllowed());
    }

    return builder.build();
  }
}
