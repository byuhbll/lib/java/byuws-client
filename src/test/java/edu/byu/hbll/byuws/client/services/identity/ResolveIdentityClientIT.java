package edu.byu.hbll.byuws.client.services.identity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.byu.hbll.byuws.client.ITUtils;
import edu.byu.hbll.byuws.client.model.identity.Identifiers;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/** Integration tests for {@link ResolveIdentityClient}. */
@ExtendWith(HoverflyExtension.class)
@HoverflySimulate(enableAutoCapture = true)
class ResolveIdentityClientIT {

  private static final ResolveIdentityClient client =
      new ResolveIdentityClient(ITUtils.byuwsClient());

  /**
   * Verifies that {@link ResolveIdentityClient#resolveNetId(String)} will return the appropriate
   * identifiers.
   *
   * @throws Exception if the request fails
   */
  @Test
  public void shouldResolveNetId() {
    String netId = "ddaisy";
    Identifiers identifiers = client.resolveNetId(netId);
    assertEquals(netId, identifiers.getNetId());
    assertEquals("595632384", identifiers.getByuId());
    assertEquals("009246042", identifiers.getPersonId());
  }
}
