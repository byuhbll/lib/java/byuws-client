package edu.byu.hbll.byuws.client.services.academic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.byuws.client.ByuwsClientException;
import edu.byu.hbll.byuws.client.ITUtils;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/** Integration tests for {@link AcademicRecordsClient}. */
@ExtendWith(HoverflyExtension.class)
@HoverflySimulate(enableAutoCapture = true)
public class AcademicRecordsClientIT {

  private static AcademicRecordsClient client = new AcademicRecordsClient(ITUtils.byuwsClient());

  /**
   * Verifies that calling {@link AcademicRecordsClient#getRecords(String)} will return a JsonNode
   * containing academic record data.
   *
   * @throws ByuwsClientException if the web service fails
   */
  @Test
  public void shouldRetrieveAcademicRecords() throws ByuwsClientException {
    String user = "ddaisy";

    JsonNode academicRecords = client.getRecords(user);
    System.out.println(academicRecords);

    assertEquals("Non-Matriculated", academicRecords.path("classStanding").asText(null));
  }
}
