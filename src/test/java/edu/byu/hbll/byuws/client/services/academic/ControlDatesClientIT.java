package edu.byu.hbll.byuws.client.services.academic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.byu.hbll.byuws.client.ByuwsClientException;
import edu.byu.hbll.byuws.client.ITUtils;
import edu.byu.hbll.byuws.client.model.academic.ControlDates;
import edu.byu.hbll.byuws.client.model.academic.ControlDates.Type;
import edu.byu.hbll.byuws.client.model.academic.Semester;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/** Integration tests for {@link ControlDatesClient}. */
@ExtendWith(HoverflyExtension.class)
@HoverflySimulate(enableAutoCapture = true)
public class ControlDatesClientIT {

  private static ControlDatesClient client = new ControlDatesClient(ITUtils.byuwsClient());

  /**
   * Verifies that {@link ControlDatesClient#controlDates(LocalDate, Type...)} returns the
   * appropriate control dates for a date.
   *
   * @throws ByuwsClientException if the web services fails
   */
  @Test
  public void shouldReturnControlDatesForDate() throws ByuwsClientException {
    List<ControlDates> response =
        client.controlDates(LocalDate.parse("2015-01-01"), Type.CURRICULUM);

    assertEquals(1, response.size());
    assertEquals(Type.CURRICULUM, response.get(0).getType());
    assertEquals(Semester.fromDescription("Fall 2014"), response.get(0).getSemester());
    assertEquals(LocalDateTime.parse("2014-09-02T00:00:00"), response.get(0).getStartDate());
    assertEquals(
        LocalDateTime.parse("2015-01-04T23:59:59.999999999"), response.get(0).getEndDate());
  }

  /**
   * Verifies that {@link ControlDatesClient#controlDates(Semester, Type...)} returns the
   * appropriate control dates for a semester.
   *
   * @throws ByuwsClientException if the web services fails
   */
  @Test
  public void shouldReturnControlDatesForTerm() throws ByuwsClientException {
    List<ControlDates> response =
        client.controlDates(Semester.fromDescription("Fall 2014"), Type.CURRICULUM);

    assertEquals(Type.CURRICULUM, response.get(0).getType());
    assertEquals(Semester.fromDescription("Fall 2014"), response.get(0).getSemester());
    assertEquals(LocalDateTime.parse("2014-09-02T00:00:00"), response.get(0).getStartDate());
    assertEquals(
        LocalDateTime.parse("2015-01-04T23:59:59.999999999"), response.get(0).getEndDate());
  }
}
