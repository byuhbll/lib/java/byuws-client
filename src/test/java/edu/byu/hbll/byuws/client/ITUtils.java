package edu.byu.hbll.byuws.client;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.json.YamlLoader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ITUtils {

  private static final Path TEST_CONFIG = Paths.get("src/test/resources/test.yml");
  private static final Path EXAMPLE_TEST_CONFIG = Paths.get("src/test/resources/test.example.yml");
  private static final JsonNode config;

  static {
    // If a test.yml has been defined (likely for making a new Hoverfly recording), use it.
    // Otherwise, use the example config because the clientId and clientSecret won't actually
    // matter (because we're relying on prerecorded responses via Hoverfly).
    Path testConfig = Files.exists(TEST_CONFIG) ? TEST_CONFIG : EXAMPLE_TEST_CONFIG;
    YamlLoader yamlLoader = new YamlLoader();
    try {
      config = yamlLoader.load(testConfig);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  public static ByuwsClient byuwsClient() {
    return ByuwsClient.OBJECT_MAPPER.treeToValue(config.path("byuwsClient"), ByuwsClient.class);
  }
}
