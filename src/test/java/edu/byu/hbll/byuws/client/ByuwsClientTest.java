package edu.byu.hbll.byuws.client;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.net.URI;
import java.time.ZoneId;
import org.junit.jupiter.api.Test;

/** Unit tests for {@link ByuwsClient}. */
public class ByuwsClientTest {

  /**
   * Verifies that supplying a null clientId to {@link ByuwsClient#builder(String, String)} will
   * throw a {@link NullPointerException}.
   */
  @Test
  public void builderShouldRejectNullClientId() {
    assertThrows(NullPointerException.class, () -> ByuwsClient.builder(null, "clientSecret"));
  }

  /**
   * Verifies that supplying a null clientSecret to {@link ByuwsClient#builder(String, String)} will
   * throw a {@link NullPointerException}.
   */
  @Test
  public void builderShouldRejectNullClientSecret() {
    assertThrows(NullPointerException.class, () -> ByuwsClient.builder("clientId", null));
  }

  /**
   * Verifies that supplying a null baseUri to {@link ByuwsClient.Builder#baseUri(URI)} will throw a
   * {@link NullPointerException}.
   */
  @Test
  public void builderShouldRejectNullBaseURI() {
    assertThrows(
        NullPointerException.class,
        () -> ByuwsClient.builder("clientId", "clientSecret").baseUri(null));
  }

  /**
   * Verifies that supplying a null providerTimezone to {@link
   * ByuwsClient.Builder#providerTimezone(ZoneId)} will throw a {@link NullPointerException}.
   */
  @Test
  public void builderShouldRejectNullProviderTimezone() {
    assertThrows(
        NullPointerException.class,
        () -> ByuwsClient.builder("clientId", "clientSecret").providerTimezone(null));
  }
}
