package edu.byu.hbll.byuws.client.services.identity;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.byuws.client.ByuwsClientException;
import edu.byu.hbll.byuws.client.ITUtils;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/** Integration tests for {@link IdentityClient}. */
@ExtendWith(HoverflyExtension.class)
@HoverflySimulate(enableAutoCapture = true)
public class IdentityClientIT {

  private static final IdentityClient client = new IdentityClient(ITUtils.byuwsClient());

  /**
   * Verifies that {@link IdentityClient#personSummary(String)} will return the appropriate person
   * summary.
   *
   * @throws ByuwsClientException if the request fails
   */
  @Test
  public void shouldReturnPersonSummary() {
    String netId = "ddaisy";

    JsonNode personSummary = client.personSummary(netId);
    assertEquals(netId, personSummary.path("identifiers").path("net_id").asText(null));
  }

  /**
   * Verifies that {@link IdentityClient#personNames(String)} will return the appropriate person
   * names.
   *
   * @throws ByuwsClientException if the request fails
   */
  @Test
  public void shouldReturnPersonNames() {
    String netId = "ddaisy";
    String surname = "Duck";
    String preferredName = "Daisy";

    JsonNode personNames = client.personNames(netId);
    assertEquals(surname, personNames.path("official_name").path("surname").asText(null));
    assertEquals(
        preferredName,
        personNames.path("preferred_name").path("preferred_first_name").asText(null));
  }
}
