package edu.byu.hbll.byuws.client.model.academic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.byu.hbll.byuws.client.model.academic.Semester.Term;
import edu.byu.hbll.json.ObjectMapperFactory;
import java.time.LocalDateTime;
import java.time.Year;
import org.junit.jupiter.api.Test;

/** Unit tests for {@link ControlDates}. */
public class ControlDatesTest {

  private static final String example =
      "{"
          + "\"end_date\": \"20170501 23:59:59\","
          + "\"description\": \"1st day of cls to day before next 1st day of cls\","
          + "\"year_term_desc\": \"Winter 2017\","
          + "\"year_term\": \"20171\","
          + "\"start_date\": \"20170109 00:00:00\","
          + "\"date_type\": \"CURRICULUM\""
          + "}";

  /** Verifies that Jackson is able to deserialize a control dates entity. */
  @Test
  public void shouldDeserialize() {
    ControlDates dates = ObjectMapperFactory.newUnchecked().readValue(example, ControlDates.class);

    assertEquals("1st day of cls to day before next 1st day of cls", dates.getDescription());
    assertEquals(LocalDateTime.parse("2017-01-09T00:00:00"), dates.getStartDate());
    assertEquals(LocalDateTime.parse("2017-05-01T23:59:59.999999999"), dates.getEndDate());
    assertEquals(new Semester(Term.WINTER, Year.of(2017)), dates.getSemester());
  }
}
