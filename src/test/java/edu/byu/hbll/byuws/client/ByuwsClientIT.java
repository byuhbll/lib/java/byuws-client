package edu.byu.hbll.byuws.client;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/** Integration tests for {@link ByuwsClient}. */
@ExtendWith(HoverflyExtension.class)
// Use the first annotation below during simulation.  When recording new Hoverfly responses,
// however, use the second annotation instead.
@HoverflySimulate(enableAutoCapture = true)
// @HoverflySimulate(enableAutoCapture = true,  config = @HoverflyConfig(statefulCapture = true))
public class ByuwsClientIT {

  /**
   * Verifies that attempting to login with valid credentials will return an access token.
   *
   * @throws ByuwsClientException if the web service fails
   */
  @Test
  public void loginShouldSucceedWithValidCredentials() throws ByuwsClientException {
    ByuwsClient client = ITUtils.byuwsClient();
    client.login();
    assertNotNull(client.getAccessToken());
  }

  /**
   * Verifies that attempting to login with bogus credentials will throw a {@link
   * ByuwsClientException}.
   *
   * <p>This test is currently disabled because WireMock doesn't appear to properly match requests
   * based on basic authentication.
   *
   * @throws ByuwsClientException if the web service fails
   */
  @Test
  public void loginShouldFailWithBogusCredentials() {
    ByuwsClient client = ITUtils.byuwsClient().withCredentials("bogusId", "bogusSecret");
    assertThrows(ByuwsClientException.class, () -> client.login());
  }
}
