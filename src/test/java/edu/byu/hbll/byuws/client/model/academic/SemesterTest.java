package edu.byu.hbll.byuws.client.model.academic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/** Unit tests for {@link Semester}. */
public class SemesterTest {

  /**
   * Verifies that calling {@link Semester#fromDescription(String)} with a valid description will
   * return the appropriate semester and return the appropriate code and semester.
   */
  @Test
  public void shouldParseValidDescription() {
    Semester winter1999 = Semester.fromDescription("Winter 1999");
    assertEquals("19991", winter1999.getCode());
    assertEquals("Winter 1999", winter1999.getDescription());

    Semester spring2000 = Semester.fromDescription("Spring 2000");
    assertEquals("20003", spring2000.getCode());
    assertEquals("Spring 2000", spring2000.getDescription());

    Semester summer2001 = Semester.fromDescription("Summer 2001");
    assertEquals("20014", summer2001.getCode());
    assertEquals("Summer 2001", summer2001.getDescription());

    Semester fall2002 = Semester.fromDescription("Fall 2002");
    assertEquals("20025", fall2002.getCode());
    assertEquals("Fall 2002", fall2002.getDescription());
  }

  /**
   * Verifies that calling {@link Semester#fromDescription(String)} with a null description will
   * throw an {@link NullPointerException}.
   */
  @Test
  public void shouldFailToParseNullDescription() {
    assertThrows(NullPointerException.class, () -> Semester.fromDescription(null));
  }

  /**
   * Verifies that calling {@link Semester#fromDescription(String)} with an invalid description will
   * throw an {@link IllegalArgumentException}.
   */
  @Test
  public void shouldFailToParseInvalidDescription() {
    assertThrows(IllegalArgumentException.class, () -> Semester.fromDescription("Bogus 2015"));
  }

  /**
   * Verifies that calling {@link Semester#fromCode(String)} with a valid code will return the
   * appropriate semester and return the appropriate code and semester.
   */
  @Test
  public void shouldParseFromValidCode() {
    Semester winter1999 = Semester.fromCode("19991");
    assertEquals("19991", winter1999.getCode());
    assertEquals("Winter 1999", winter1999.getDescription());

    Semester spring2000 = Semester.fromCode("20003");
    assertEquals("20003", spring2000.getCode());
    assertEquals("Spring 2000", spring2000.getDescription());

    Semester summer2001 = Semester.fromCode("20014");
    assertEquals("20014", summer2001.getCode());
    assertEquals("Summer 2001", summer2001.getDescription());

    Semester fall2002 = Semester.fromCode("20025");
    assertEquals("20025", fall2002.getCode());
    assertEquals("Fall 2002", fall2002.getDescription());
  }

  /**
   * Verifies that calling {@link Semester#fromCode(String)} with a null code will throw an {@link
   * NullPointerException}.
   */
  @Test
  public void shouldFailToParseNullCode() {
    assertThrows(NullPointerException.class, () -> Semester.fromCode(null));
  }

  /**
   * Verifies that calling {@link Semester#fromCode(String)} with an invalid code will throw an
   * {@link IllegalArgumentException}.
   */
  @Test
  public void shouldFailToParseInvalidCode() {
    assertThrows(IllegalArgumentException.class, () -> Semester.fromCode("201512345"));
  }

  /**
   * Verifies that calling {@link Semester#equals(Object)} will return {@code true} if the provided
   * Semester is a different object but represents the same semester.
   */
  @Test
  public void equalsShouldReturnTrueWhenOtherSemesterIsEquivalent() {
    Semester a = Semester.fromCode("19911"); // Winter 1991
    Semester b = Semester.fromDescription("Winter 1991");

    assertTrue(a.equals(b));
  }

  /**
   * Verifies that calling {@link Semester#equals(Object)} will return {@code false} if the provided
   * Semester represents a different semester.
   */
  @Test
  public void equalsShouldReturnFalseWhenSemesterDoesNotEvaluateAsTheSame() {
    Semester a = Semester.fromCode("19911"); // Winter 1991
    Semester b = Semester.fromDescription("Fall 1991");

    assertFalse(a.equals(b));
  }
}
