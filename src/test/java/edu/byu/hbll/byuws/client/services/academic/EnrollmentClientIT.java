package edu.byu.hbll.byuws.client.services.academic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.byu.hbll.byuws.client.ByuwsClientException;
import edu.byu.hbll.byuws.client.ITUtils;
import edu.byu.hbll.byuws.client.model.academic.Enrollment;
import edu.byu.hbll.byuws.client.model.academic.Semester;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/** Integration tests for {@link EnrollmentClient}. */
@ExtendWith(HoverflyExtension.class)
@HoverflySimulate(enableAutoCapture = true)
class EnrollmentClientIT {

  private static EnrollmentClient client = new EnrollmentClient(ITUtils.byuwsClient());

  /**
   * Verifies that calling {@link EnrollmentClient#courseScheduleForNetId(String, Semester...)} will
   * return a JsonNode containing course schedule data.
   *
   * @throws ByuwsClientException if the web service fails
   */
  @Test
  public void courseScheduleForNetIdShouldRetrieveCourseSchedule() throws ByuwsClientException {
    Enrollment academicRecords =
        client.courseScheduleForNetId("ddaisy", Semester.fromDescription("Summer 2019"));

    assertEquals(1, academicRecords.getStudentClassCount());
    assertEquals(0, academicRecords.getInstructorClassCount());
  }

  /**
   * Verifies that calling {@link EnrollmentClient#courseScheduleForNetId(String, Semester...)} will
   * return a JsonNode containing course schedule data.
   *
   * @throws ByuwsClientException if the web service fails
   */
  @Test
  public void courseScheduleForNetIdShouldRetrieveCourseSchedulesForMultipleTerms()
      throws ByuwsClientException {

    Enrollment academicRecords =
        client.courseScheduleForNetId(
            "ddaisy",
            Semester.fromDescription("Spring 2019"),
            Semester.fromDescription("Summer 2019"));

    assertEquals(2, academicRecords.getStudentClassCount());
    assertEquals(0, academicRecords.getInstructorClassCount());
  }

  /**
   * Verifies that calling {@link EnrollmentClient#courseScheduleForNetId(String, Semester...) without defining at
   * least 1 semester will throw an {@link IllegalArgumentException}.
   *
   * @throws ByuwsClientException if the web service fails
   */
  @Test
  public void courseScheduleForNetIdShouldFailIfNoSemestersSpecified() throws ByuwsClientException {
    assertThrows(IllegalArgumentException.class, () -> client.courseScheduleForNetId("ddaisy"));
  }
}
