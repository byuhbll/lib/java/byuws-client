package edu.byu.hbll.byuws.client.services.academic;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.byuws.client.ITUtils;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/** DceEnrollmentClientIT */
@ExtendWith(HoverflyExtension.class)
@HoverflySimulate(enableAutoCapture = true)
public class DceEnrollmentClientIT {

  private static DceEnrollmentClient client = new DceEnrollmentClient(ITUtils.byuwsClient());

  @Test
  public void shouldReturnTrue() throws Exception {
    assertTrue(
        client.isEnrolledInIndependentStudy("ddaisy2"),
        "User should be enrolled in Independent Study");
  }

  @Test
  public void shouldReturnFalse() throws Exception {
    assertFalse(
        client.isEnrolledInIndependentStudy("ddaisy"),
        "User should not be enrolled in Independent Study");
  }
}
