package edu.byu.hbll.byuws.client.services.access;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.byuws.client.ByuwsClientException;
import edu.byu.hbll.byuws.client.ITUtils;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/** Integration tests for {@link IsMemberClient}. */
@ExtendWith(HoverflyExtension.class)
@HoverflySimulate(enableAutoCapture = true)
public class IsMemberClientIT {

  private static IsMemberClient client = new IsMemberClient(ITUtils.byuwsClient());

  /**
   * Verifies that {@link IsMemberClient#isMember(String, String)} returns {@code true} if the given
   * user is a member of the given group.
   *
   * @throws ByuwsClientException if the web service fails
   */
  @Test
  public void isMemberShouldReturnTrueIfPersonIsMemberOfGroup() throws ByuwsClientException {
    assertTrue(client.isMember("aerstd", "ddaisy2"));
  }

  /**
   * Verifies that {@link IsMemberClient#isMember(String, String)} returns {@code false} if the
   * given user is a not a member of the given group.
   *
   * @throws ByuwsClientException if the web service fails
   */
  @Test
  public void isMemberShouldReturnFalseIfPersonIsNotMemberOfGroup() throws ByuwsClientException {
    assertFalse(client.isMember("aerstd", "ddaisy"));
  }
}
