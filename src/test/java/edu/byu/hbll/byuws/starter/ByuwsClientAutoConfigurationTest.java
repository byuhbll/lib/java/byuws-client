package edu.byu.hbll.byuws.starter;

import static org.assertj.core.api.Assertions.assertThat;

import edu.byu.hbll.byuws.client.ByuwsClient;
import java.net.URI;
import java.time.ZoneId;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

public class ByuwsClientAutoConfigurationTest {

  private final ApplicationContextRunner contextRunner =
      new ApplicationContextRunner()
          .withPropertyValues(
              "byuhbll.byuws-client.clientId=testID",
              "byuhbll.byuws-client.clientSecret=superSecret",
              "byuhbll.byuws-client.baseUri=www.example.com",
              "byuhbll.byuws-client.zoneId=America/Denver")
          .withConfiguration(AutoConfigurations.of(ByuwsClientAutoConfiguration.class));

  @Test
  public void clientExists() {
    this.contextRunner.run((context) -> assertThat(context).hasSingleBean(ByuwsClient.class));
  }

  @Test
  public void shouldPreferUserProvidedBean() {
    this.contextRunner
        .withUserConfiguration(ByuwsClientTestUserConfig.class)
        .run(
            (context) ->
                assertThat(context.getBean(ByuwsClient.class).getProviderTimezone())
                    .isEqualTo(ZoneId.of("America/Phoenix")));
  }

  @Test
  public void shouldNotCrashWhenNotUsingAutoconfiguration() {
    new ApplicationContextRunner()
        .withConfiguration(AutoConfigurations.of(ByuwsClientAutoConfiguration.class))
        .run(context -> assertThat(context).doesNotHaveBean(ByuwsClientAutoConfiguration.class));
  }

  @Configuration
  static class ByuwsClientTestUserConfig {
    @Bean
    ByuwsClient byuwsClient() {
      return ByuwsClient.builder("otherID", "someSecret")
          .baseUri(URI.create("www.example.com"))
          .providerTimezone(ZoneId.of("America/Phoenix"))
          .build();
    }
  }
}
