# BYU Web Services Client (Java)

This web service client library provides support for using the university web services provided by BYU's Office of IT.

## Usage

Add this library as a dependency to your project's pom.xml.

```xml
<dependency>
  <groupId>edu.byu.hbll</groupId>
  <artifactId>byuws-client</artifactId>
  <version>1.2.0</version>
</dependency>
```

### Core Client

The core of the library is the **ByuwsClient** class, which provides a thread-safe container for configuration as well as request methods that automatically manage authentication and retries.  The configuration can be defined programmatically using the provided **ByuwsClient.Builder** class; it can also be loaded from your application configuration.  The following parameters can be set:

- `clientId` (required): The OAuth2 Client ID used for authentication.
- `clientSecret` (required): The OAuth2 Client Secret used for authentication.
- `rootUri` (optional): The base URI for the BYU web services (defaults to `https://api.byu.edu`).
- `providerTimezone` (optional): The timezone from which the BYU web services are being hosted (defaults to `America/Denver`).
- `attemptsAllowed` (optional): BYU's web services are prone to random failures.  As a result, this client library includes support for automatic retries of GET requests and login attempts (but not write requests).  This is the total number of attempts allowed for those types of requests (defaults to `3`; set to `1` to disable retries altogether).

The following YAML snippet provides a template for defining BYU web services configuration declaratively:

```yaml
byuws:
  # Required variables.
  clientId: myClient
  clientSecret: mySecret
  # Optional variables. 
  rootUri: "https://api.byu.edu"
  providerTimezone: "America/Denver"
  attemptsAllowed: 3
```

Assuming that you have already loaded this YAML data into a **JsonNode**, you can map it to a new **ByuwsClient** object using the Jackson **ObjectMapper**:

```java
JsonNode config; // Containing the full YAML above.
ObjectMapper objectMapper = new ObjectMapper();
objectMapper.treeToValue(config.path("byuws"), ByuwsClient.class);
```

Once a **ByuwsClient** has been constructed, you can invoke web services requests by using the `exchange` method.  If you already have an unexpired OAuth2 session, it will be reused during this invocation.  Otherwise, this method will automatically retrieve new credentials from the BYU-WS login service.  Note that this method expects a properly formatted JSON response from the invoked web service.  However, some legacy services at BYU do not use UTF-8 encoding in their responses, as is [required by the JSON specification](https://tools.ietf.org/html/rfc8259#section-8.1).  Use the `exchangeIso8859Json` method instead when interacting with those services to ensure that the responses are parsed properly.

### Extended Clients

For convenience, a number of extended clients have been included for commonly-used BYU web services.  These extended clients are [facades](https://en.wikipedia.org/wiki/Facade_pattern) for the core **ByuwsClient** that provide endpoint-appropriate methods and response handling and are located in the `edu.byu.hbll.byuws.client.services` package (and its subclasses).  Like the **ByuwsClient** itself, all of the extended clients are thread-safe and only need to be initialized once per application.

The following example demonstrates how to use an extended client to make a request to the Person Summary service:

```java
ByuwsClient byuwsClient = ByuwsClient.builder("myClientId", "myClientSecret").build();
IdentityClient identityClient = new IdentityClient(byuwsClient);
JsonNode response = identityClient.personSummary("ddaisy");

// You can optionally convert this JSON response into a POJO by using an ObjectMapper.
PersonSummary persumm = new ObjectMapper().treeToValue(response, PersonSummary.class);
```

The current release includes support for the following web services as extended clients:

- [AcademicRecords](https://api.byu.edu/store/apis/info?name=AcademicRecordsPublicInfo&version=v1&provider=BYU%2Fdkeele5)
- [ControlDates](https://api.byu.edu/store/apis/info?name=AcademicControlsControlDatesWS&version=v1&provider=BYU%2Fbrogerm)
- [DceEnrollment](https://api.byu.edu/store/apis/info?name=DCEEnrollStatus&version=v1.0&provider=BYU%2Fbdh8)
- [Enrollment](https://api.byu.edu/store/apis/info?name=AcademicRegistrationEnrollment&version=v1&provider=BYU%2Ftrevash)
- [IsMember](https://api.byu.edu/store/apis/info?name=IdentityAccessIsMember&version=v1&provider=BYU%2Ftrevash)
- Identity (Legacy PRO)
  - [PersonSummary](https://api.byu.edu/store/apis/info?name=IdentityPersonPROPersonSummary&version=v1&provider=BYU%2Ftrevash)
  - [PersonNames](https://api.byu.edu/store/apis/info?name=IdentityPersonPROPersonNames&version=v1&provider=BYU%2Ftrevash)
- [ResolveIdentity](https://api.byu.edu/store/apis/info?name=IdentityPersonResolveIdentity&version=v1&provider=BYU%2Ftrevash)

If you have a use-case for another service, you can build your own extended client within your application or use the **ByuwsClient** directly.  If you choose to build your own extended client, please feel free to submit it for future inclusion in the library.

## Tests

This project uses [Hoverfly](https://hoverfly.io/) for mocking web services calls to the BYU web services.  If you need to regenerate the Hoverfly recordings, you will need to perform some cleanup operations afterwards.

### ByuwsClientIT

**ByuwsClientIT** requires both a successful (200) and a failed (401) login recording.  You may need to temporarily replace the provided `@HoverflySimulate` annotation with the following declaration:

```java
@HoverflySimulate(enableAutoCapture = true, config = @HoverflyConfig(statefulCapture = true))
```

This will save a recording for each call to the OAuth2 login service.  After the recordings are saved, revert the change to the `@HoverflySimulate` annotation.  Then open `src/test/resources/edu_byu_hbll_byuws_client_ByuwsClientIT.json` and remove all but two recordings (one of the failures and one of the sucessful logins).  

Depending on which recordings you removed, continue your cleanup by removing references to `transitionsState` (in the response object) or `requiresState` (in the request object).  Add the following filter to the the request entity for the failure (which will listen for the bogus credentials used to validated failure behavior and ensure that the appropriate recording is returned):

```json
"headers" : {
  "Authorization" : [ {
    "matcher" : "exact",
    "value" : "Basic Ym9ndXNJZDpib2d1c1NlY3JldA=="
  } ]
}
```

Finally, change the value of the key returned by the successful service to some random value.

### DceEnrollmentClientIT

The **DceEnrollmentClientIT** verifies that both users enrolled and unenrolled in Independent Study classes are handled properly.  Since we do not want to accidentally disclose protected information about a real person, be sure to use a fake account (such as the `ddaisy` BYU testing account) for this test.

At the time that this library was written, `ddaisy` was *not* "enrolled" in any Independent Study classes, so we use that recording without change to verify the behavior of positive case.  To handle the case of a student who *is* enrolled in Independent Study, copy the response from your fake user, and change the response `body` field to read `true`.  Then change the username in both the recording and the test code (make sure not to change it to a real user's ID).

Finally, sanitize the tokens.  First replace the value of the OAuth2 bearer token returned by the login service to some random value.  Then change the `CFTOKEN` value in the `Set-Cookie` header of the responses from the DCE service.

### EnrollmentClientIT

The **EnrollmentClientIT** verifies the interaction with the enrollment service for traditional classes.  As this service returns more details about the enrollment than the DceEnrollment service, it is correspondingly more difficult to mock.  To our knowledge, none of the BYU test accounts are "enrolled" in any courses at the university.  Instead, the delivered recording was made using a real-life student at BYU.  When re-recording this service, take special care to ensure that no real user information is left behind in the recording.  Change names, email addresses, course numbers and codes, and professor information!

After sanitizing the recording, go back to the **EnrollmentClientIT** class and replace the real-life Net ID with the fake value you used in the sanitized recording.  Also change the value of the `Sm_user` header in the response to ensure that it shows a fake or test user.  Finally, replace the value of the OAuth2 bearer token returned by the login service to some random value.

### IsMemberClientIT

Similar to the EnrollmentClientIT, the **IsMemberClientIT** has proven difficult to test using test users only (which tend to not have any group memberships).  Accordingly, the current recordings for the `isMember=true` use case were made by running a real-life student through, and then sanitizing their information.

If you choose to re-record this service, ensure that the Net ID of the test user is replaced with a fake Net ID.  Also make sure to change the value of the `Sm_user` header in the response to ensure that it shows a fake or test user.  After sanitizing the recording, be sure to return to the integration test and replace the Net ID with the test value you applied to the recording.  Finally, replace the value of the OAuth2 bearer token returned by the login service with a random value.

### Other

For other recordings, change the value of the `Sm_user` header (if it is set) in the response to ensure that it shows a fake or test user.  Then replace the value of the OAuth2 bearer token returned by the login service.  The value of the bearer token does not matter for the tests, so you can enter some random text here.  While these tokens will eventually expire, manually effecting this change safeguards that committing new recordings while the token is still valid will not accidentally expose session credentials to the world.

## Developing This Project

Note: These instructions assume Docker and VS Code, with the Dev Containers extension, are installed.

Git clone the project locally and open it with VS Code. When prompted, reopen the project in a dev container. It will take a couple of minutes to download all of the dependencies and prepare the container for Java development. Proceed with development once the initialization is complete.

Tests can be run from within VS Code. Click the Testing icon and click "Run Tests".

To build and install the project locally in order to include it in another project for testing, run the following from the project directory on the host machine.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn -f /project/pom.xml -Duser.home=/var/maven clean package install
```

Building and deploying this project is handled by the GitLab pipeline. There are two possible destinations for the built project: the BYU HBLL internal Maven repository or the central Maven repository. Use the internal repository for non-public releases and the central repository for public open source releases. To instruct the pipeline to deploy to Maven Central, set the `MAVEN_CENTRAL_DEPLOY` CI/CD variable to `true`. Deploying to either the internal or central repository requires manually playing the deploy job. Note: Deployments to Maven Central are permanent.

## License

[License](LICENSE.md)